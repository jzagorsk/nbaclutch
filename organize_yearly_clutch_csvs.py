from datetime import datetime, timedelta
import csv
import math

BASE_URL = 'https://www.basketball-reference.com'
WEIGHTS_FOR_YEAR = {2002: [0.27515, -1.03416, 1.05431, -1.10521, 0.73653, 0.15086, -0.63678],
                    2003: [0.36796, -0.68469, 0.922, -1.01766, 0.54617, 0.09987, -0.69429],
                    2004: [0.40409, -0.81194, 0.77226, -0.82998, 0.4768, 0.06184, -0.48047],
                    2005: [0.21747, -0.96769, 0.94524, -1.00618, 0.65548, 0.14204, -0.558],
                    2006: [0.40617, -0.78706, 0.89274, -0.87905, 0.58287, 0.05205, -0.68894],
                    2007: [0.07095, -0.50397, 0.81203, -0.84788, 0.87861, 0.07078, -0.55226],
                    2008: [0.26914, -0.64157, 0.71518, -0.72613, 0.5226, 0.05659, -0.53367],
                    2009: [0.29932, -0.94502, 1.01479, -0.99348, 0.94743, 0.06544, -0.47558],
                    2010: [0.26625, -0.82758, 0.81549, -0.87796, 0.52932, 0.11613, -0.61309],
                    2011: [0.29814, -0.87235, 0.70948, -0.7648, 0.53208, 0.09409, -0.56079],
                    2012: [0.14818, -0.79763, 0.70881, -0.71774, 0.48438, 0.11618, -0.50873],
                    2013: [0.24144, -0.6004, 0.89192, -0.9664, 0.81453, 0.08997, -0.54454],
                    2014: [0.15531, -0.71656, 0.77586, -0.79066, 0.51492, 0.13344, -0.52063],
                    2015: [0.28343, -0.88433, 0.82872, -0.88685, 0.69868, 0.0915, -0.32049],
                    2016: [0.17614, -0.78434, 0.83194, -0.78981, 0.65681, 0.09361, -0.44161],
                    2017: [0.19096, -0.8837, 0.91343, -1.02373, 0.83631, 0.13142, -0.2969],
                    2018: [0.23289, -0.7663, 0.98116, -0.97988, 0.9798, 0.09706, -0.56345]}


def get_sorted_dates_in_year(year):
    file_name = 'pbp_{}.csv'.format(year)
    date_set = set()
    with open(file_name, 'rb') as f:
        for line in f:
            row = line.split(',')
            if row[0] == 'player_id':
                continue
            date_obj = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
            date_set.add(date_obj)

    sorted_dates = sorted(date_set)
    min = sorted_dates[0]
    max = sorted_dates[-1]
    x = min
    new_dates_set = set()
    new_dates_set.add(x)
    while x != max:
        x += timedelta(days=1)
        new_dates_set.add(x)
    return sorted(new_dates_set)


def construct_player_games_played_dict_for_year(year):
    d = dict()
    file_name = 'pbp_{}.csv'.format(year)
    with open(file_name, 'rb') as f:
        for line in f:
            row = line.split(',')
            if row[0] == 'player_id':
                continue
            date_obj = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
            player_id = int(row[0])
            time_interval = int(row[5])
            point_diff = int(row[6])
            if not meets_condition(point_diff, time_interval):
                continue

            if player_id in d:
                d[player_id].add(date_obj)
            else:
                d[player_id] = {date_obj}
    return d


def meets_condition(point_diff, time_diff):
    return (abs(point_diff) <= 6 and 10 < time_diff <= 60) \
           or (abs(point_diff) <= 3 and 0 <= time_diff <= 10)


def make_player_dict(year):
    dict_to_return = {}
    file_name = 'pbp_{}.csv'.format(year)
    with open(file_name, 'rb') as f:
        for line in f:
            row = line.split(',')
            if row[0] == 'player_id':
                continue
            player_id = int(row[0])
            date_of_game = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
            time_interval = int(row[5])
            point_diff = int(row[6])
            stat = row[8]
            num_stat = int(row[9])
            if not meets_condition(point_diff, time_interval):
                continue

            if player_id not in dict_to_return:
                dict_to_return[player_id] = {date_of_game:
                                                          {stat: num_stat}}
            else:
                d = dict_to_return[player_id]
                if date_of_game not in d:
                    d[date_of_game] = {stat: num_stat}
                else:
                    d = d[date_of_game]
                    if stat not in d:
                        d[stat] = num_stat
                    else:
                        d[stat] += num_stat
    return dict_to_return


def player_stats_in_year_up_to_date(sorted_dates, player_stats_to_date_dict):
    resulting_dict = {}
    for player_id in player_stats_to_date_dict:
        date_stat_dict = player_stats_to_date_dict[player_id]
        for d in sorted_dates:
            for key in date_stat_dict:
                if key >= d:
                    continue
                stat_dict = date_stat_dict[key]
                for stat in stat_dict:
                    if player_id not in resulting_dict:
                        resulting_dict[player_id] = \
                            {d: {stat: stat_dict[stat]}}
                    else:
                        if d not in resulting_dict[player_id]:
                            resulting_dict[player_id][d] = \
                                {stat: stat_dict[stat]}
                        else:
                            if stat not in resulting_dict[player_id][d]:
                                resulting_dict[player_id][d][stat] = \
                                    stat_dict[stat]
                            else:
                                resulting_dict[player_id][d][stat] += \
                                    stat_dict[stat]
            if player_id not in resulting_dict:
                resulting_dict[player_id] = {d: None}
            else:
                if d not in resulting_dict[player_id]:
                    resulting_dict[player_id][d] = None
    return resulting_dict


def calc_index(stats, year):
    index = 0.0
    weights = WEIGHTS_FOR_YEAR[year][1:]
    index += WEIGHTS_FOR_YEAR[year][0]
    for i in range(len(weights)):
        index += weights[i]*stats[i]
    return index


def make_id_to_url_dict(year):
    id_to_url_dict = {}
    file_name = 'players_{}.csv'.format(year)
    with open(file_name, 'rb') as f:
        for line in f:
            row = line.strip().split(',')
            if row[0] == 'id':
                continue
            player_id = int(row[0])
            url = row[1]
            id_to_url_dict[player_id] = url
    return id_to_url_dict


def compute_num_games_to_date(player_dates, date_obj):
    return len([d for d in player_dates if d < date_obj])


def get_player_image_url(player_url):
    player_name = player_url.replace('.html', '')
    new_name = []
    for i in range(len(player_name)):
        if i == 9 or i == 10:
            continue
        new_name.append(player_name[i])
    new_name = ''.join(new_name)
    base = 'https://d2cwpp38twqe55.cloudfront.net/req/201712064/images'
    image_url = '{}{}.jpg'.format(base, new_name)
    return image_url


def read_player_url_to_name_dict():
    url_name_dict = {}
    with open('url_names.csv', 'rb') as f:
        for line in f:
            row = line.strip().split(',')
            if row[0] == 'url':
                continue
            url_name_dict[row[0]] = row[1]
    return url_name_dict


def main():
    url_to_name_dict = read_player_url_to_name_dict()
    print 'Done making url_to_name_dict'
    stat_list = ['Assist', 'Defensive Rebound', 'Miss',
                 'Offensive Rebound', 'Points', 'Turnover']
    for year in range(2002, 2019, 1):
        print 'Working on {}'.format(year)
        id_url_dict = make_id_to_url_dict(year)
        sorted_dates_in_season = get_sorted_dates_in_year(year)
        player_num_games_dict = construct_player_games_played_dict_for_year(year)
        for key in player_num_games_dict:
            player_num_games_dict[key] = len(player_num_games_dict[key])
        player_stats_per_date_dict = make_player_dict(year)
        stats_up_to_date = player_stats_in_year_up_to_date(sorted_dates_in_season,
                                                           player_stats_per_date_dict)
        data_to_write = [['name', 'index', 'image_url']]
        d = sorted_dates_in_season[-1]
        for player in stats_up_to_date:
            player_stats = stats_up_to_date[player][d]
            if player_stats is None:
                continue
            row_for_index = []
            for s in stat_list:
                row_for_index.append(player_stats[s]*1.0/player_num_games_dict[player]
                                     if s in player_stats else 0)
            index = calc_index(row_for_index, year)
            exp_index = math.exp(index)
            p_index = exp_index/(1 + exp_index)
            data_to_write.append([url_to_name_dict[id_url_dict[player]],
                                  p_index, get_player_image_url(id_url_dict[player])])
        with open('condensed_up_to_date_{}.csv'.format(year), 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(data_to_write)
        print 'Done with {}'.format(year)


if __name__ == '__main__':
    main()
