import csv

GAME_STAT_DICT = {}
STATS_SET = set()


def meets_condition(point_diff, time_diff):
    return (abs(point_diff) <= 6 and 10 < time_diff <= 60) \
           or (abs(point_diff) <= 3 and 0 <= time_diff <= 10)


def bucket_year(year):
    with open('pbp_{}.csv'.format(year), 'rb') as f:
        reader = csv.reader(f)
        reader.next()
        done = False
        while not done:
            try:
                row = reader.next()
                if int(row[5]) > 60:
                    continue
                point_diff = int(row[6])
                time_diff = int(row[5])
                if not meets_condition(point_diff, time_diff):
                    continue
                game_tuple = (row[7], row[2], row[3], row[4])
                stat = row[8]
                STATS_SET.add(stat)
                quantity = int(row[9])
                if game_tuple in GAME_STAT_DICT:
                    d = GAME_STAT_DICT[game_tuple]
                    if stat in d:
                        d[stat] += quantity
                    else:
                        d[stat] = quantity
                else:
                    GAME_STAT_DICT[game_tuple] = {stat: quantity}
            except StopIteration:
                done = True

    sorted_stats = sorted(STATS_SET)
    with open('clutch_bucket_{}.csv'.format(year), 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(['Team', 'Date', 'Final Diff', 'Win Loss']
                        + sorted_stats)
        for tup in GAME_STAT_DICT:
            row_to_write = [tup[0], tup[1], tup[2]]
            row_to_write.append(1 if tup[3] == 'W' else 0)
            stat_dict = GAME_STAT_DICT[tup]
            for s in sorted_stats:
                if s in stat_dict:
                    row_to_write.append(stat_dict[s])
                else:
                    row_to_write.append(0)
            writer.writerow(row_to_write)


def main():
    for year in range(2001, 2019, 1):
        bucket_year(year)
        GAME_STAT_DICT.clear()


if __name__ == '__main__':
    main()
