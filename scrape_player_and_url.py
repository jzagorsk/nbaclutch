from bs4 import BeautifulSoup
import requests
import csv


TEAMS_URL = 'https://www.basketball-reference.com/leagues/NBA_{year}.html'
BASE_URL = 'https://www.basketball-reference.com'


def make_url_to_name_dict():
    url_to_name_dict = {}
    for year in range(2002, 2019, 1):
        print year
        content = requests.get(TEAMS_URL.format(year=year)).content
        soup = BeautifulSoup(content, 'html.parser')
        tables = soup.findAll('table', {'class': 'suppress_all sortable stats_table'})
        teams = tables[0].findAll('a') + tables[1].findAll('a')
        for t in teams:
            print t
            team_page = requests.get(BASE_URL + t['href']).content
            soup2 = BeautifulSoup(team_page, "html.parser")
            roster = soup2.findAll('table', {'id': "roster"})[0]
            names = roster.findAll('td', {'data-stat': 'player'})
            for n in names:
                player = n.findAll('a')[0]
                url_to_name_dict[player['href']] = player.text
    with open('url_names.csv', 'wb') as f:
        wr = csv.writer(f)
        rows = [['url', 'name']]
        for key in url_to_name_dict:
            rows.append([key, url_to_name_dict[key]])
        wr.writerows(rows)


def main():
    make_url_to_name_dict()


if __name__ == '__main__':
    main()
