from bs4 import BeautifulSoup
from datetime import datetime
import sys
import requests
import math
import re
import csv

GLOBAL_DICT = {}
PLAYER_DICT = {}

MONTH_SET = {'october', 'november', 'december', 'january', 'february',
             'march', 'april', 'may', 'june'}
RAW_URL = 'https://www.basketball-reference.com'

sample = "https://www.basketball-reference.com/boxscores/pbp/201701010IND.html"
all_games_in_month_url = ('https://www.basketball-reference.com/leagues/'
                          'NBA_{year}_games-{month}.html')

SCORE_REGEX = re.compile('(\d{1,})-(\d{1,})')
SCORING_PLAY_CLASS = 'bbr-play-score'
NUM_COLS_IN_PBP_TABLE = 6
NET_POINT_REGEX = re.compile('\+(\d{1})')
DATE_FROM_URL_REGEX = re.compile('\d{8}')


def parse_play_by_play(url):
    boxscore = requests.get(url).content
    soup = BeautifulSoup(boxscore,"html.parser")
    table = soup.find(id='pbp')
    if table is None:
        return
    away_home_scores = soup.findAll('div', {'class': 'score'})
    away_score = int(away_home_scores[0].text)
    home_score = int(away_home_scores[1].text)
    final_score_diff = home_score - away_score
    team_names = soup.findAll('a', {'itemprop': 'name'})
    away_team_name = team_names[0].text
    home_team_name = team_names[1].text
    rows = table.findAll('tr')
    index = 0
    for row in rows:
        th = row.findAll('th')
        if len(th) == 0:
            index += 1
            continue
        if th[0].text != '4th Q':
            index += 1
            continue
        rows = rows[index:]
        for row in rows:
            columns = row.findAll('td')
            if len(columns) != 6:
                continue
            time_col = columns[0]
            d = None
            try:
                d = datetime.strptime(time_col.text, '%M:%S.%f')
            except ValueError:
                print "Error"
            minutes = d.minute
            seconds = d.second
            num_seconds_left = minutes*60 + seconds
            score_cell = columns[3].text
            away_home_points = re.findall(SCORE_REGEX, score_cell)[0]
            away_score = int(away_home_points[0])
            home_score = int(away_home_points[1])
            # home - away
            point_diff = home_score - away_score
            if abs(point_diff) > 10:
                continue
            away_result = columns[1]
            home_result = columns[5]
            away_net_points = columns[2].text
            home_net_points = columns[4].text
            raw_date_from_url = re.findall(DATE_FROM_URL_REGEX, url)[0]
            date_from_url =  datetime.strptime(raw_date_from_url, '%Y%m%d')
            parse_result_cell(away_result, num_seconds_left, -point_diff,
                              away_net_points, date_from_url,
                              -final_score_diff, away_team_name)
            parse_result_cell(home_result, num_seconds_left, point_diff,
                              home_net_points, date_from_url,
                              final_score_diff, home_team_name)


def get_games_from_year_month(year, month):
    url = all_games_in_month_url.format(year=year, month=month)
    content = requests.get(url).content
    soup = BeautifulSoup(content, "html.parser")
    all_box_score_link_cells = soup.findAll('td',
                                            {'data-stat':"box_score_text"})
    box_score_urls = []
    for td in all_box_score_link_cells:
        if len(td.findAll('a')) == 0:
            continue
        box_score_url = '%s%s' % (RAW_URL, td.find('a')['href'])
        splits = box_score_url.split('boxscores/')
        new_score = splits[0] + 'boxscores/pbp/' + splits[1]
        box_score_urls.append(new_score)
    return box_score_urls


def in_range(num_seconds_left, low, high):
    return high > num_seconds_left >= low


def get_num_seconds_interval(num_seconds_left):
    if in_range(num_seconds_left, 0, 5):
        return int(math.ceil(num_seconds_left))
    if in_range(num_seconds_left, 5, 30):
        a = int(math.floor(num_seconds_left))
        return a - a%5 + 5
    if in_range(num_seconds_left, 30, 300):
        a = int(math.floor(num_seconds_left))
        return a - a%30 + 30
    return -1


def handle_stat(player, date_from_url, final_point_diff, num_seconds_left,
                point_diff, stat, num_stat, team_name):
    num_seconds_left = get_num_seconds_interval(num_seconds_left)
    if num_seconds_left == -1:
        return
    win_loss_str = (final_point_diff, 'W') if final_point_diff > 0 \
        else (final_point_diff, 'L')
    if player not in GLOBAL_DICT:
        PLAYER_DICT[player] = (len(PLAYER_DICT) + 1, team_name)
        GLOBAL_DICT[player] = {date_from_url:
                                   {win_loss_str:
                                       {num_seconds_left:
                                            {point_diff:
                                                 {team_name:
                                                    {stat: num_stat}}}}}}
    else:
        d = GLOBAL_DICT[player]
        if date_from_url not in d:
            d[date_from_url] = {win_loss_str:
                                    {num_seconds_left:
                                        {point_diff:
                                             {team_name:
                                                {stat: num_stat}}}}}
        else:
            d = d[date_from_url]
            if win_loss_str not in d:
                d[win_loss_str] = {num_seconds_left:
                                           {point_diff:
                                                {team_name:
                                                    {stat: num_stat}}}}
            else:
                d = d[win_loss_str]
                if num_seconds_left not in d:
                    d[num_seconds_left] = {point_diff:
                                               {team_name:
                                                    {stat: num_stat}}}
                else:
                    d = d[num_seconds_left]
                    if point_diff not in d:
                        d[point_diff] = {team_name: {stat: num_stat}}
                    else:
                        d = d[point_diff]
                        if team_name not in d:
                            d[team_name] = {stat: num_stat}
                        else:
                            d = d[team_name]
                            if stat not in d:
                                d[stat] = num_stat
                            else:
                                d[stat] += num_stat


def get_net_points(net_point_string):
    matches = re.findall(NET_POINT_REGEX, net_point_string)
    return int(matches[0][0])


def parse_result_cell(result_cell, num_seconds_left, point_diff, net_points,
                      date_from_url, final_point_diff, team_name):
    cell_text = result_cell.text
    if cell_text.strip() == '':
        return
    if result_cell.has_attr("class"):
        if result_cell['class'][0] == SCORING_PLAY_CLASS:
            players = result_cell.findAll('a')
            points_scored = get_net_points(net_points)
            if len(players) > 0:
                handle_stat(players[0]['href'], date_from_url,
                            final_point_diff,
                            num_seconds_left, point_diff, 'Points',
                            points_scored, team_name)
                if len(players) == 2:
                    handle_stat(players[1]['href'], date_from_url,
                                final_point_diff,
                                num_seconds_left, point_diff, 'Assist', 1,
                                team_name)
        elif cell_text.startswith('Offensive rebound by'):
            players = result_cell.findAll('a')
            if len(players) > 0:
                handle_stat(players[0]['href'], date_from_url,
                            final_point_diff,
                            num_seconds_left, point_diff,
                            'Offensive Rebound', 1, team_name)
        elif cell_text.startswith('Defensive rebound by'):
            players = result_cell.findAll('a')
            if len(players) > 0:
                handle_stat(players[0]['href'], date_from_url,
                            final_point_diff,
                            num_seconds_left, point_diff, 'Defensive Rebound',
                            1, team_name)
        elif cell_text.startswith('Turnover by'):
            players = result_cell.findAll('a')
            if len(players) > 0:
                handle_stat(players[0]['href'], date_from_url,
                            final_point_diff,
                            num_seconds_left, point_diff, 'Turnover', 1,
                            team_name)
        elif 'misses 2-pt' in result_cell.text or 'misses 3-pt' \
                in result_cell.text:
            players = result_cell.findAll('a')
            handle_stat(players[0]['href'], date_from_url,
                        final_point_diff, num_seconds_left,
                        point_diff, 'Miss', 1, team_name)
        else:
            pass


def main():
    if len(sys.argv) != 3:
        print "Need to enter start year and end year"
        exit(0)
    for year in range(int(sys.argv[1]), int(sys.argv[2]), 1):
        for month in sorted(MONTH_SET):
            games = get_games_from_year_month(year, month)
            for g in games:
                print year, month, g
                parse_play_by_play(g)

        with open('pbp_{}.csv'.format(year), "wb") as myfile:
            wr = csv.writer(myfile)
            wr.writerow(['player_id', 'year', 'date', 'final_point_diff',
                         'win_loss', 'time_interval', 'point_differential',
                         'team_name', 'stat', 'quantity'])
            for player in GLOBAL_DICT:
                for date in GLOBAL_DICT[player]:
                    for wl in GLOBAL_DICT[player][date]:
                        for num in GLOBAL_DICT[player][date][wl]:
                            for pd in GLOBAL_DICT[player][date][wl][num]:
                                for tn in GLOBAL_DICT[player][date][wl][num][pd]:
                                    for stat in GLOBAL_DICT[player][date][wl][num][pd][tn]:
                                        a = GLOBAL_DICT[player][date][wl][num][pd][tn][stat]
                                        wr.writerow([PLAYER_DICT[player][0], year, date, wl[0], wl[1],
                                                     num, pd, tn, stat, a])
        GLOBAL_DICT.clear()
        with open('players_{}.csv'.format(year), "wb") as f:
            wr = csv.writer(f)
            wr.writerow(['id', 'name'])
            for player in PLAYER_DICT:
                wr.writerow([PLAYER_DICT[player][0], player])
        PLAYER_DICT.clear()


if __name__ == '__main__':
    main()
