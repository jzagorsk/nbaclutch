

def get_top_10_and_bot_10():
    file_name = 'condensed_up_to_date_2017.csv'
    mydict = {}
    with open(file_name, 'rb') as f:
        for line in f:
            row = line.strip().split(',')
            if row[0] == 'name':
                continue
            mydict[row[0]] = float(row[1])
    sorted_values = sorted(mydict.iteritems(), key=lambda (k, v): (v, k))
    bot_10 = sorted([(v, k) for (k, v) in sorted_values[0:10]])
    top_10 = [(v, k) for (k, v) in sorted_values[-10:]]
    return top_10, bot_10

def main():
    (top_10, bot_10) = get_top_10_and_bot_10()
    print top_10
    print bot_10

if __name__ == '__main__':
    main()
